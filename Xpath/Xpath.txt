1. �� ������� �������� ����� http://stackoverflow.com/ �������� XPath ��� (������� �� ������ https://goo.gl/Yg3Mzd ):

a) ����� header:
.//div[@class='-container']

b) ���� � ����������� �� ������� featured ����� ���-��������:
.//a/span[@class="bounty-indicator-tab"]  ��� .//a[2]/span

c) ������� ���� ���-�������� � ������� ��������:
.//a[@class='question-hyperlink']

d) ������� ���� ��������, ���������� ����� �How� �� ������ ���-��������:
.//a[@class='question-hyperlink' and contains(., 'How')]

e) ������� ���� ��������, ������� ����� ��� �java�:  
.//div[@class='summary' and contains(., 'java')]//a[@class='question-hyperlink']

2. �� �������� http://www.seleniumhq.org/ �������� ��� ��������, ������� �������� � �������������� ������ (������� alt) ����� 'Selenium' ��� �logo�:
.//img[contains(@alt, 'Selenium') or contains(@alt, 'logo')]

3. �� �������� http://www.seleniumhq.org/ ������� ����� �� �������� ������, ������� �������� ��������� �� �������:
.//div[@id='footer']//ul/li/ul/li[position()>2]

4. �� �������� https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=yellow+duck+&rh=i%3Aaps%2Ck%3Ayellow+duck+  �����:

a) ��� ����� �� ������, ���������� ����� Prime:
.//div[@class='s-item-container' and .//i[contains(@class, 'a-icon-prime')]]//div//a/h2

b) ��� ����� �� ������, �� ���������� ����� Prime � ����� ����, ��� �� ���������� ��������:
.//div[@class='s-item-container' and .//i[not(contains(@class, 'a-icon-prime'))]]//div//a/h2


5. �� �������� http://www.amazon.com/ � ������ ����� � ������ ����� ����� ������:

a) �� ��� ������ ����� �������� (�������� ����� ��������� �� �������): 
.//div[contains(@class, 'navFooterLine')]/ul/li[position()>6]/a


b) ��� ������ ����� ��������, ������� ��������:
.//div[contains(@class, 'navFooterLine')]/ul/li[position()>=6]/a