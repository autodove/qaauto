package com.welcome;

//import java.lang.System;

/*
 * This class stores the users name,
 * displays the greetings
 */
public class Hello {
    private int month;
    private String name = "";

    public void setupName(String name) {
        this.name = name;
    }

    public boolean setMonth(int month) {
        if((month < 1)||(month > 12))
        {
            System.out.println(month + "Ошибочка вышла: введите month от 1 до 12, ");
            return false;
        }
        this.month = month;
        return true;
    }

   public int getMonth(){
       return month;
   }

    public void welcome() {
        System.out.println("Hello, " + name);
    }

    public void byeBay() {
        System.out.println("Bye, " + name);
    }

}

