package com.figure;


public class Triangle extends Figure {

    public double x1;
    public double y1;
    public double x2;
    public double y2;
    public double x3;
    public double y3;

    public Triangle(double x1, double y1,
                    double x2, double y2,
                    double x3, double y3) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
    }

    @Override
    public void move(double dx, double dy){
        this.x1 += dx;
        this.y1 += dy;
        this.x2 += dx;
        this.y2 += dy;
        this.x3 += dx;
        this.y3 += dy;
    }

    @Override
    public double area() {
        double a = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        double b = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2));
        double c = Math.sqrt(Math.pow(x3 - x1, 2) + Math.pow(y3 - y1, 2));
        double p = 0.5 * (a + b + c);
        double ar = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        return ar;
    }

    @Override
    public void resize(double scalingFactor){
        this.x1 *= scalingFactor;
        this.y1 *= scalingFactor;
        this.x2 *= scalingFactor;
        this.y2 *= scalingFactor;
        this.x3 *= scalingFactor;
        this.y3 *= scalingFactor;
    }

}
