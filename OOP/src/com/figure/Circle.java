package com.figure;

public class Circle extends Figure {
    public double xTopLeft;
    public double yTopLeft;
    public double diameter;

    public Circle(double xTopLeft, double yTopLeft, double diameter) {
        this.xTopLeft = xTopLeft;
        this.yTopLeft = yTopLeft;
        this.diameter = diameter;
    }

    @Override
    public void move(double dx, double dy){
        this.xTopLeft += dx;
        this.yTopLeft += dy;
    }

    @Override
    public double area() {
        return 0.25 * Math.PI * diameter * diameter;
    }

    @Override
    public void resize(double scalingFactor){
        diameter *= scalingFactor;
    }

}
