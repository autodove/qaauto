package com.figure;


public class Square extends Figure {
    public double xTopLeft;
    public double yTopLeft;
    public double side;

    public Square(double xCenter, double yCenter, double side) {
        this.xTopLeft = xTopLeft;
        this.yTopLeft = yTopLeft;
        this.side = side;
    }

    @Override
    public void move(double dx, double dy){
        this.xTopLeft += dx;
        this.yTopLeft += dy;
    }

    @Override
    public double area() {
        return side * side;
    }

    @Override
    public void resize(double scalingFactor){
        side *= scalingFactor;
    }

}
