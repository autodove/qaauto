package com.nixsolutions;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;
import com.figure.Circle;
import com.figure.Figure;
import com.figure.Square;
import com.figure.Triangle;

public class Main {

    public static void main(String[] args) {
        final int FIGURES_COUNT = 10;
        final int FIGURES_TYPE_COUNT = 3;
        final int FIGURES_MAX_SIZE = 100;

        // create figures
        Figure figures[] = new Figure[FIGURES_COUNT];
        Random random = new Random();
        for (int i = 0; i < FIGURES_COUNT; i++)
        {
            int figureType = random.nextInt(FIGURES_TYPE_COUNT);
            int x = random.nextInt(FIGURES_MAX_SIZE) + 1;
            int y = random.nextInt(FIGURES_MAX_SIZE) + 1;
            int size = random.nextInt(FIGURES_MAX_SIZE) + 1;
            switch (figureType)
            {
                case 0:
                    figures[i] = new Circle(x, y, size);
                    break;
                case 1:
                    figures[i] = new Square(x, y, size);
                    break;
                case 2:
                    figures[i] = new Triangle(x, y, x + size, y + size, 2 * x, 2 * y);
                    break;
                default:
                    break;
            }
        }

        // resize figures
        for (int i = 0; i < FIGURES_COUNT; i++) {
            figures[i].resize(Math.random());
        }

        // print figures area before sort
        System.out.println("Figures area before sort");
        for (int i = 0; i < FIGURES_COUNT; i++) {
            String figureType = "Circle";
            if(figures[i] instanceof Square) {
                figureType = "Square";
            }
            else if (figures[i] instanceof Triangle) {
                figureType = "Triangle";
            }
            System.out.println(figureType + ", area: " + figures[i].area());
        }

        // sort figures
        Arrays.sort(figures, new Comparator<Figure>() {
            public int compare(Figure f1, Figure f2) {
                return (int) (f1.area() - f2.area());
            }
        });

        // print figures area after sort
        System.out.println("\nFigures area after sort");
        for (int i = 0; i < FIGURES_COUNT; i++) {
            String figureType = "Circle";
            if(figures[i] instanceof Square) {
                figureType = "Square";
            }
            else if (figures[i] instanceof Triangle) {
                figureType = "Triangle";
            }
            System.out.println(figureType + ", area: " + figures[i].area());
        }

    }
}
