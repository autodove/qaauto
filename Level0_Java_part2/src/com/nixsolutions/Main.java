package com.nixsolutions;

import java.util.Scanner;
import com.calculator.Arithmetic;
import com.games.Matrix;
import com.games.Palindrome;
import com.games.Snail;

public class Main {

    public static int inputInteger(String inputMessage, String errorMessage) {
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.print(inputMessage);
            String str = input.nextLine();
            try {
                return Integer.parseInt(str);
            }
            catch (NumberFormatException ex) {
                System.out.print(errorMessage);
                continue;
            }
        }
    }

    public static double inputDouble(String inputMessage, String errorMessage) {
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.print(inputMessage);
            String str = input.nextLine();
            try {
                return Double.parseDouble(str);
            }
            catch (NumberFormatException ex) {
                System.out.print(errorMessage);
                continue;
            }
        }
    }

    public static void printArray(int array[][]) {
        for (int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[i].length; j++) {
                System.out.print(String.format("%4d", array[i][j]));
            }
            System.out.println();
        }
    }

    public static void main(String[] args) throws Exception {
        String errorMessage = "Input error! ";

        // Matrix
        while (true) {
            int matrixSize = inputInteger("Please, input number in range [1;9]: ", errorMessage);
            if (matrixSize < 1 || matrixSize > 9) {
                System.out.print(errorMessage);
                continue;
            }
            int matrix[][] = Matrix.createMatrix(matrixSize);
            printArray(matrix);

            System.out.println("Please, press <X> for loop exit or any key to continue: ");
            Scanner input = new Scanner(System.in);
            String str = input.nextLine();
            if (str.equals("x") || str.equals("X")) {
                break;
            }
        }

        // Arithmetic
        //1) array multiplication
        int size = inputInteger("Please, input array size: ", errorMessage);
        double array[] = new double[size];
        for (int i = 0; i < size; ++i) {
            array[i] = inputDouble("input array[" + i + "]: ", errorMessage);
        }
        System.out.println("array multiplication = " + Arithmetic.arrayMultiplication(array));
        //2) power
        double x = inputDouble("Please, input power base: ", errorMessage);
        double y = inputDouble("Please, input power exponent: ", errorMessage);
        System.out.println("base^exponent = " + Arithmetic.power(x, y));
        //3) division
        x = inputDouble("Please, input divided: ", errorMessage);
        y = inputDouble("Please, input divider: ", errorMessage);
        System.out.println("divided / divider = " + Arithmetic.division(x, y));
        //4) root
        x = inputDouble("Please, input value: ", errorMessage);
        System.out.println("root(value) = " + Arithmetic.root(x));

        // Snail
        size = inputInteger("Please, input number > 3 : ", errorMessage);
        int snail[][] = Snail.calculateSnail(size);
        printArray(snail);

        // Palindrome
        Scanner input = new Scanner(System.in);
        System.out.print("Please, input word: ");
        String str = input.nextLine();
        boolean isPalindrome = Palindrome.checkWord(str);
        if (isPalindrome) {
            System.out.println("Word is palindrome!");
        } else {
            System.out.println("Word is not palindrome!");
        }

        System.out.print("Please, input phrase: ");
        str = input.nextLine();
        isPalindrome = Palindrome.checkPhrase(str);
        if (isPalindrome) {
            System.out.println("Phrase is palindrome!");
        } else {
            System.out.println("Phrase is not palindrome!");
        }

    }
}