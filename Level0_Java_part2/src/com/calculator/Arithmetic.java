package com.calculator;


public class Arithmetic {
    public static double arrayMultiplication(double array[]) {
        double mul = array[0];
        for (int i = 1; i < array.length; i++)
        {
            mul *= array[i];
        }
        return mul;
    }

    public static double power(double base, double exponent) {
        return Math.pow(base, exponent);
    }

    public static double division(double dividend, double divider) {
        return dividend / divider;
    }

    public static double root(double x) {
        return Math.sqrt(x);
    }
}
