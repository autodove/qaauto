package com.games;


public class Snail {
    public static int[][] calculateSnail(int size) {
        int array[][] = new int[size][size];
        int row = 0;
        int column = size;
        int delta = -1;
        int lastColumn = 0;
        int lastRow = size - 1;
        if ((size % 2) == 0){
            row = size-1;
            column = -1;
            delta = 1;
            lastColumn = size - 1;
            lastRow = 0;
        }
        int number = size * size;
        // fill row
        while (column != lastColumn) {
            column += delta;
            array[row][column] = number--;
        }
        while (number > 0) {
            // fill column
            while (row != lastRow) {
                row -= delta;
                array[row][column] = number--;
            }
            delta = -delta;
            int temp = lastColumn;
            lastColumn = lastRow;
            lastRow = temp + delta;
            // fill row
            while (column != lastColumn) {
                column += delta;
                array[row][column] = number--;
            }
        }
        return array;
    }
}
