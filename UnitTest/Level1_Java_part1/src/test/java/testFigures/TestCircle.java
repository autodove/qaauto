package testFigures;

import com.figure.Circle;
import org.junit.*;

public class TestCircle {

    private static Circle circle;
    private static final double X = 0;
    private static final double Y = 0;
    private static final double DIAMETER = 10;

    // Create figure, which will be used in tests
    @BeforeClass
    public static void CreateCircleInstance() {
        circle = new Circle(X, Y, DIAMETER);
    }

    // Initialize figure before each test
    @Before
    public void setupStartCondition() {
        circle.xTopLeft = X;
        circle.yTopLeft = Y;
        circle.diameter = DIAMETER;
    }

    // Check figure moving by x coord
    @Test
    public void checkMoveByX(){
        circle.move(10, 0);
        Assert.assertTrue("Expected x coord: 10, and actual result is " + circle.xTopLeft, circle.xTopLeft == 10);
    }

    // Check figure moving by y coord
    @Test
    public void checkMoveByY(){
        circle.move(0, 100);
        Assert.assertTrue("Expected y coord: 100, and actual result is " + circle.yTopLeft, circle.yTopLeft == 100);
    }

    // Check figure resizing
    @Test
    public void checkResizing(){
        double coef = 0.5;
        circle.resize(coef);
        Assert.assertFalse("Expected size: 5, and actual result is " + circle.diameter,
                circle.diameter != DIAMETER * coef);
    }

}
