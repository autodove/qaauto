package testFigures;

import org.junit.*;
import com.figure.Square;

public class TestSquare {

    private static Square square;
    private static final double X = 50;
    private static final double Y = 0;
    private static final double SIDE = 10;
    private final double AREA = SIDE * SIDE;

    // Create figure, which will be used in tests
    @BeforeClass
    public static void CreateSquareInstance() {
        square = new Square(X, Y, SIDE);
    }

    // Initialize figure before each test
    @Before
    public void setupStartCondition() {
        square.xTopLeft = X;
        square.yTopLeft = Y;
        square.side = SIDE;
    }

    // Check figure moving by x coord
    @Test
    public void checkMoveByX(){
        square.move(10, 0);
        Assert.assertTrue("Expected x coord: 60, and actual result is " + square.xTopLeft, square.xTopLeft == 60);
    }

    // Check figure moving by y coord
    @Test
    public void checkMoveByY(){
        square.move(0, 100);
        Assert.assertTrue("Expected y coord: 100, and actual result is " + square.yTopLeft, square.yTopLeft == 100);
    }

    // Check area computing
    @Test
    public void checkAreaComputing(){
        double area = square.area();
        Assert.assertFalse("Expected area: 100, and actual result is " + area, area != AREA);
    }

    // Check figure resizing
    @Test
    public void checkResizing(){
        double coef = 0.5;
        square.resize(coef);
        Assert.assertFalse("Expected size: 5, and actual result is " + square.side, square.side != SIDE * coef);
    }

}