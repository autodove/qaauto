package testGames;

import org.junit.*;
import com.games.Matrix;

public class TestMatrix {

    // Check matrix rows count
    @Test
    public void checkMatrixRowsCount(){
        final int SIZE = 3;
        int array[][] = Matrix.createMatrix(SIZE);
        int rows = array.length;
        Assert.assertFalse("Expected matrix rows count: 3, and actual result is " + rows, rows != SIZE);
    }

    // Check matrix columns count
    @Test
    public void checkMatrixColumnsCount(){
        final int SIZE = 4;
        int array[][] = Matrix.createMatrix(SIZE);
        int cols = SIZE;
        for (int i = 0; i < array.length; i++) {
            if (array[i].length != SIZE) {
                cols = array[i].length;
                break;
            }
        }
        Assert.assertTrue("Expected matrix columns count: 4, and actual result is " + cols, cols == SIZE);
    }

    // Check if matrix elements values are in range [1;9]
    @Test
    public void checkMatrixElementsRange(){
        final int SIZE = 3;
        int array[][] = Matrix.createMatrix(SIZE);
        boolean inRange = true;
        int value = 0;
        for (int i = 0; (i < array.length) && inRange; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if ((array[i][j] < 1) || (array[i][j] > 9)) {
                    inRange = false;
                    value = array[i][j];
                }
            }
        }
        Assert.assertTrue("Expected matrix elements values in range [1;9], and actual result is " + value, inRange);
    }

}
