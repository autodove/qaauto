package testGames;

import org.junit.*;
import com.games.Snail;

public class TestSnail {

    // Check if snail min element value is equal to 1
    @Test
    public void checkMatrixMinElementValue(){
        int minimum = 0;
        for(int size = 4; size < 10; size++) {
            int array[][] = Snail.calculateSnail(size);
            minimum = array[0][0];
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++) {
                    if (minimum > array[i][j]) {
                        minimum = array[i][j];
                    }
                }
            }
            if (minimum != 1){
                break;
            }
        }
        Assert.assertTrue("Expected matrix min element value is " + 1 + ", and actual result is " + minimum,
                minimum == 1);
    }

    // Check if snail max element value is equal to sizeMatrix * sizeMatrix
    @Test
    public void checkMatrixMaxElementValue(){
        int maximum = 0;
        int MAX = 0;
        for(int size = 4; size < 10; size++) {
            MAX = size * size;
            int array[][] = Snail.calculateSnail(size);
            maximum = array[0][0];
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++) {
                    if (maximum < array[i][j]) {
                        maximum = array[i][j];
                    }
                }
            }
            if (maximum != MAX){
                break;
            }
        }
        Assert.assertTrue("Expected matrix max element value is " + MAX + ", and actual result is " + maximum,
                maximum == MAX);
    }

    // Check snail rows count
    @Test
    public void checkSnailRowsCount(){
        final int SIZE = 6;
        int array[][] = Snail.calculateSnail(SIZE);
        int rows = array.length;
        Assert.assertFalse("Expected snail rows count: 6, and actual result is " + rows, rows != SIZE);
    }

    // Check snail columns count
    @Test
    public void checkSnailColumnsCount(){
        final int SIZE = 7;
        int array[][] = Snail.calculateSnail(SIZE);
        int cols = SIZE;
        for (int i = 0; i < array.length; i++) {
            if (array[i].length != SIZE) {
                cols = array[i].length;
                break;
            }
        }
        Assert.assertFalse("Expected snail columns count: 7, and actual result is " + cols, cols != SIZE);
    }

}
