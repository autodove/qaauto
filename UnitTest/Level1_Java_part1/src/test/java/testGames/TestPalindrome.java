package testGames;

import org.junit.*;
import com.games.Palindrome;


public class TestPalindrome {

    // Check case insensitive of checking word-palindrome algorithm
    @Test
    public void checkWordCaseInsensitivity(){
        String word = "Abba";
        boolean isPalindrome = Palindrome.checkWord(word);
        Assert.assertTrue("Expected result: \"Abba\" is palindrome, and actual result is not palindrome",
                isPalindrome);
    }

    // Check word-palindrome with even count of chars
    @Test
    public void checkWordEvenCharsCount(){
        String word = "asddsa";
        boolean isPalindrome = Palindrome.checkWord(word);
        Assert.assertTrue("Expected result: \"asddsa\" is palindrome, and actual result is not palindrome",
                isPalindrome);
    }

    // Check word-palindrome with odd count of chars
    @Test
    public void checkWordOddCharsCount(){
        String word = "qwewq";
        boolean isPalindrome = Palindrome.checkWord(word);
        Assert.assertTrue("Expected result: \"qwewq\" is palindrome, and actual result is not palindrome",
                isPalindrome);
    }

    // Check word-nonpalindrome
    @Test
    public void checkWordNonpalindrome(){
        String word = "qwerty";
        boolean isPalindrome = Palindrome.checkWord(word);
        Assert.assertFalse("Expected result: \"qwerty\" is not palindrome, and actual result is palindrome",
                isPalindrome);
    }

    // Check phrase-palindrome with space
    @Test
    public void checkPhraseWithSpace(){
        String phrase = " Madam  Im   Adam ";
        boolean isPalindrome = Palindrome.checkPhrase(phrase);
        Assert.assertTrue("Expected result: \" Madam  Im   Adam \" is palindrome, and actual result is not palindrome",
                isPalindrome);
    }

    // Check phrase-nonpalindrome
    @Test
    public void checkPhraseNonpalindrome(){
        String phrase = "Hello World";
        boolean isPalindrome = Palindrome.checkPhrase(phrase);
        Assert.assertFalse("Expected result: \"Hello World\" is not palindrome, and actual result is palindrome",
                isPalindrome);
    }

    // Check phrase consists of 1 word
    @Test
    public void checkOneWordPhrase(){
        String phrase = "radar";
        boolean isPalindrome = Palindrome.checkPhrase(phrase);
        Assert.assertTrue("Expected result: \"radar\" is palindrome, and actual result is not palindrome",
                isPalindrome);
    }

}
