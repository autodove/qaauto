package com.games;


public class Matrix {
    private static final int MAX_SIZE = 9;
    public static int[][] createMatrix(int size) {
        int array[][] = new int[size][size];
        int number = 1;
        for (int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                array[i][j] = number;
                if (++number > MAX_SIZE) {
                    number = 1;
                }
            }
        }
        return array;
    }
}
