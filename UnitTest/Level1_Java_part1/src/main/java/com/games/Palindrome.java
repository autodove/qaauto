package com.games;


public class Palindrome {
    public static boolean checkWord(String someWord) {
        String word = someWord.toLowerCase();
        int first = 0;
        int last = word.length() - 1;
        while(first < last) {
            if (word.charAt(first) != word.charAt(last)){
                return false;
            }
            first++;
            last--;
        }
        return true;
    }

    public static boolean checkPhrase(String somePhrase) {
        return checkWord(somePhrase.replaceAll(" ", ""));
    }

}
