package com.figure;


public abstract class Figure {
    public abstract void move(double dx, double dy);
    public abstract double area();
    public abstract void resize(double scalingFactor);
}
