@Palindrome
Feature: Palindrome

  @Palindrome
  Scenario: 001 Check case insensitive of checking word-palindrome algorithm
    When I use checkWord method with argument word is "Abba"
    Then result is "true"

  @Palindrome
  Scenario: 002 Check word-palindrome with even count of chars
    When I use checkWord method with argument word is "asddsa"
    Then result is "true"

  @Palindrome
  Scenario: 003 Check word-palindrome with odd count of chars
    When I use checkWord method with argument word is "qwewq"
    Then result is "true"

  @Palindrome
  Scenario: 004 Check word-nonpalindrome
    When I use checkWord method with argument word is "qwerty"
    Then result is "false"

  @Palindrome
  Scenario: 005 Check phrase-palindrome with space
    When I use checkPhrase method with argument word is " Madam  Im   Adam "
    Then result is "true"

  @Palindrome
  Scenario: 006 Check phrase-nonpalindrome
    When I use checkPhrase method with argument word is "Hello World"
    Then result is "false"

  @Palindrome
  Scenario: 007 Check phrase consists of 1 word
    When I use checkPhrase method with argument word is "radar"
    Then result is "true"