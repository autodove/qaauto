@Snail
Feature: Snail

  @Snail
  Scenario: 001 Check snail rows count
    When I use calculateSnail method with argument size is "4"
    Then snail rows count is "4"

  @Snail
  Scenario Outline: 002 Check snail columns count
    When I use calculateSnail method with argument size is "<value>"
    Then snail columns count is "<expectedValue>"
    Examples:
      | value | expectedValue |
      |   3   |        3      |
      |   6   |        6      |
      |   10  |       10      |

  @Snail
  Scenario: 003 Check if snail min element is equal to "1"
    When I use calculateSnail method with argument size is "7"
    Then snail minimum element is "1"

  @SnailSpecial
  Scenario Outline: 004 Check if snail max element value is equal to size * size
    When I use calculateSnail method with argument size is "<value>"
    Then snail maximum element is "<expectedValue>"
    Examples:
    | value | expectedValue |
    |   5   |       25      |
    |   8   |       64      |
    |   9   |       81      |