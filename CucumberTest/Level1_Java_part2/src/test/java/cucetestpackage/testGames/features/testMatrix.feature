@Matrix
Feature: Matrix

  @Matrix
  Scenario: 001 Check matrix rows count
    When I use createMatrix method with argument size is "3"
    Then matrix rows count is "3"

  @Matrix
  Scenario: 002 Check matrix columns count
    When I use createMatrix method with argument size is "5"
    Then matrix columns count is "5"

  @Matrix
  Scenario: 003 Check if matrix elements values are in range [1;9]
    When I use createMatrix method with argument size is "7"
    Then matrix elements values are between "1" and "9"