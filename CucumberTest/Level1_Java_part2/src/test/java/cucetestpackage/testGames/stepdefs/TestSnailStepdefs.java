package cucetestpackage.testGames.stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.games.Snail;
import org.junit.Assert;

public class TestSnailStepdefs {

    private static int snail[][] = null;

    @When("^I use calculateSnail method with argument size is \"([^\"]*)\"$")
    public void iUseCalculateSnailMethodWithArgumentSizeIs(String arg0) throws Throwable {
        snail = Snail.calculateSnail(Integer.valueOf(arg0));
    }

    @Then("^snail rows count is \"([^\"]*)\"$")
    public void snailRowsCountIs(String arg0) throws Throwable {
        Assert.assertTrue(snail.length == Integer.valueOf(arg0));
    }

    @Then("^snail columns count is \"([^\"]*)\"$")
    public void snailColumnsCountIs(String arg0) throws Throwable {
        final int SIZE = Integer.valueOf(arg0);
        int cols = SIZE;
        for (int i = 0; i < snail.length; i++) {
            if (snail[i].length != SIZE) {
                cols = snail[i].length;
                break;
            }
        }
        Assert.assertTrue(cols == SIZE);
    }

    @Then("^snail minimum element is \"([^\"]*)\"$")
    public void snailMinimumElementIs(String arg0) throws Throwable {
        int minimum = snail[0][0];
        for (int i = 0; i < snail.length; i++) {
            for (int j = 0; j < snail[i].length; j++) {
                if (minimum > snail[i][j]) {
                    minimum = snail[i][j];
                }
            }
        }
        Assert.assertTrue(minimum == Integer.valueOf(arg0));
    }

    @Then("^snail maximum element is \"([^\"]*)\"$")
    public void snailMaximumElementIs(String arg0) throws Throwable {
        int maximum = snail[0][0];
        for (int i = 0; i < snail.length; i++) {
            for (int j = 0; j < snail[i].length; j++) {
                if (maximum < snail[i][j]) {
                    maximum = snail[i][j];
                }
            }
        }
        Assert.assertTrue(maximum == Integer.valueOf(arg0));
    }
}
