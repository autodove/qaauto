package cucetestpackage.testGames.stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.games.Matrix;
import org.junit.Assert;

public class TestMatrixStepdefs {

    private static int array[][] = null;

    @When("^I use createMatrix method with argument size is \"([^\"]*)\"$")
    public void iUseCreateMatrixMethodWithArgumentSizeIs(String arg0) throws Throwable {
        array = Matrix.createMatrix(Integer.valueOf(arg0));
    }

    @Then("^matrix rows count is \"([^\"]*)\"$")
    public void matrixRowsCountIs(String arg0) throws Throwable {
        Assert.assertTrue(array.length == Integer.valueOf(arg0));
    }

    @Then("^matrix columns count is \"([^\"]*)\"$")
    public void matrixColumnsCountIs(String arg0) throws Throwable {
        final int SIZE = Integer.valueOf(arg0);
        int cols = SIZE;
        for (int i = 0; i < array.length; i++) {
            if (array[i].length != SIZE) {
                cols = array[i].length;
                break;
            }
        }
        Assert.assertTrue(cols == SIZE);
    }

    @Then("^matrix elements values are between \"([^\"]*)\" and \"([^\"]*)\"$")
    public void matrixElementsValuesInAreBetweenAnd(String arg0, String arg1) throws Throwable {
        final int MIN = Integer.valueOf(arg0);
        final int MAX = Integer.valueOf(arg1);
        boolean inRange = true;
        for (int i = 0; (i < array.length) && inRange; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if ((array[i][j] < MIN) || (array[i][j] > MAX)) {
                    inRange = false;
                    break;
                }
            }
        }
        Assert.assertTrue(inRange);
    }
}
