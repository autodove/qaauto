package cucetestpackage.testGames.stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.games.Palindrome;
import org.junit.Assert;

public class TestPalindromeStepdefs {

    private boolean isPalindrome = false;

    @When("^I use checkWord method with argument word is \"([^\"]*)\"$")
    public void iUseCheckWordMethodWithArgumentWordIs(String arg0) throws Throwable {
        isPalindrome = Palindrome.checkWord(arg0);
    }

    @When("^I use checkPhrase method with argument word is \"([^\"]*)\"$")
    public void iUseCheckPhraseMethodWithArgumentWordIs(String arg0) throws Throwable {
        isPalindrome = Palindrome.checkPhrase(arg0);
    }

    @Then("^result is \"([^\"]*)\"$")
    public void resultIs(String arg0) throws Throwable {
        Assert.assertTrue(isPalindrome == Boolean.valueOf(arg0));
    }
}
