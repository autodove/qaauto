package cucetestpackage.testGames.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/nixreport", "json:target/cucumber.json"},
        features = {"src/test/java/cucetestpackage/testGames/features"},
        glue = "cucetestpackage/testGames.stepdefs",
        tags = "@Matrix, @Palindrome, @Snail")

public class TestGamesRun {
}
