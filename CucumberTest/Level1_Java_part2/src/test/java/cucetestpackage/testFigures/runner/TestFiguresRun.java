package cucetestpackage.testFigures.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/nixreport", "json:target/cucumber.json"},
        features = {"src/test/java/cucetestpackage/testFigures/features"},
        glue = "cucetestpackage/testFigures.stepdefs",
        tags = "@Circle, @Square")

public class TestFiguresRun {

}
