package cucetestpackage.testFigures.stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.figure.Square;
import org.junit.Assert;

public class TestSquareStepdefs {

    private static Square square;
    private static final double X = 0;
    private static final double Y = 0;
    private static final double SIDE = 10;

    @Given("^I start program Square$")
    public void iStartProgramSquare() throws Throwable {
        square = new Square(X, Y, SIDE);
    }

    @And("^I setup start condition for Square$")
    public void iSetupStartCondition() throws Throwable {
        square.xTopLeft = X;
        square.yTopLeft = Y;
        square.side = SIDE;
    }

    @When("^I use for square move method with arguments x is \"([^\"]*)\" and y is = \"([^\"]*)\"$")
    public void iUseMoveMethodWithArgumentsXIsAndYIs(String arg0, String arg1) throws Throwable {
        square.move(Integer.valueOf(arg0), Integer.valueOf(arg1));
    }

    @Then("^x square coord is \"([^\"]*)\"$")
    public void xSquareCoordIs(String arg0) throws Throwable {
        Assert.assertTrue(square.xTopLeft == Double.valueOf(arg0));
    }

    @Then("^y square coord is \"([^\"]*)\"$")
    public void ySquareCoordIs(String arg0) throws Throwable {
        Assert.assertTrue(square.yTopLeft == Double.valueOf(arg0));
    }

    @When("^I use for square resize method with argument is = \"([^\"]*)\"$")
    public void iUseResizeMethodWithArgumentIs(String arg0) throws Throwable {
        square.resize(Double.valueOf(arg0));
    }

    @Then("^square side is \"([^\"]*)\"$")
    public void squareSideIs(String arg0) throws Throwable {
        Assert.assertTrue(square.side == Double.valueOf(arg0));
    }
}
