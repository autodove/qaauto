package cucetestpackage.testFigures.stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.figure.Circle;
import org.junit.Assert;

public class TestCircleStepdefs {

    private static Circle circle;
    private static final double X = 0;
    private static final double Y = 0;
    private static final double DIAMETER = 10;

    @Given("^I start program Circle$")
    public void iStartProgramCircle() throws Throwable {
        circle = new Circle(X, Y, DIAMETER);
    }

    @And("^I setup start condition for Circle$")
    public void iSetupStartCondition() throws Throwable {
        circle.xTopLeft = X;
        circle.yTopLeft = Y;
        circle.diameter = DIAMETER;
    }

    @When("^I use for circle move method with arguments x is \"([^\"]*)\" and y is = \"([^\"]*)\"$")
    public void iUseMoveMethodWithArgumentsXIsAndYIs(String arg0, String arg1) throws Throwable {
        circle.move(Integer.valueOf(arg0), Integer.valueOf(arg1));
    }

    @Then("^x circle coord is \"([^\"]*)\"$")
    public void xCircleCoordIs(String arg0) throws Throwable {
        Assert.assertTrue(circle.xTopLeft == Double.valueOf(arg0));
    }

    @Then("^y circle coord is \"([^\"]*)\"$")
    public void yCircleCoordIs(String arg0) throws Throwable {
        Assert.assertTrue(circle.yTopLeft == Double.valueOf(arg0));
    }

    @When("^I use for circle resize method with argument is = \"([^\"]*)\"$")
    public void iUseResizeMethodWithArgumentIs(String arg0) throws Throwable {
        circle.resize(Double.valueOf(arg0));
    }

    @Then("^circle diameter is \"([^\"]*)\"$")
    public void circleDiameterIs(String arg0) throws Throwable {
        Assert.assertTrue(circle.diameter == Double.valueOf(arg0));
    }
}
