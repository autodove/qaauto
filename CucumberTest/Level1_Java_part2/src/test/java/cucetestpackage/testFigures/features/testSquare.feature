@OOP
Feature: Square

  Background:
    Given I start program Square
    And I setup start condition for Square

  @Square @Nonstatic
  Scenario: 001 Check square moving by x coord
    When I use for square move method with arguments x is "10" and y is = "0"
    Then x square coord is "10"

  @Square @Nonstatic
  Scenario: 002 Check square moving by y coord
    When I use for square move method with arguments x is "0" and y is = "100"
    Then y square coord is "100"

  @Square @Nonstatic
  Scenario: 003 Check square resizing
    When I use for square resize method with argument is = "0.5"
    Then square side is "5"