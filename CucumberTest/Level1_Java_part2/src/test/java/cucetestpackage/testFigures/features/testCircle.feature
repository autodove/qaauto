@OOP
Feature: Circle

  Background:
    Given I start program Circle
    And I setup start condition for Circle

  @Circle @Nonstatic
  Scenario: 001 Check circle moving by x coord
    When I use for circle move method with arguments x is "10" and y is = "0"
    Then x circle coord is "10"

  @Circle @Nonstatic
  Scenario: 002 Check circle moving by y coord
    When I use for circle move method with arguments x is "0" and y is = "100"
    Then y circle coord is "100"

  @Circle @Nonstatic
  Scenario: 003 Check circle resizing
    When I use for circle resize method with argument is = "0.5"
    Then circle diameter is "5"